# AMD Hackintosh

## Hardware

**CPU**: AMD Ryzen 5 5600x
**GPU 1**: AMD Radeon 6600 XT
**GPU 2**: MSI GTX 1050 Ti Gaming X 4G
**RAM**: Corsair 2400mhz 2x8gb
**Motherboard**: MSI MAG X570 TOMAHAWK WIFI

## Features

### What Works

- WiFi
- Audio
- USB
- DisplayPort

### What Doesn't Work

- Graphics Acceleration (due to nvidia)
- HDMI (Dual HDMI/DP setup)

